local jni = require "jniwrap"
local ffi = require "ffi"

local logMsg = ""
local function log(fmt, ...)
	local msg = fmt:format(...)
	print(msg)
	logMsg = logMsg .. "\n" .. msg
end

function love.load()
	timer1 = 3
	timer2 = 10
	count = 0
	switched = false
	opened = false

	ffi.cdef [[
		JNIEnv *SDL_AndroidGetJNIEnv();
		jobject SDL_AndroidGetActivity();
	]]

	local env = ffi.C.SDL_AndroidGetJNIEnv()
	jni.setEnv(env)

end

function love.update(dt)
	timer1 = math.max(timer1-dt, 0)

	if timer1 == 0 then
		count = count + 1
		timer1 = 3

		-- Counting up since it is already crashing while loading a Java class
		if count == 1 then
			log("Starting Up")

			Activity = jni.wrapClass("Class/Activity.ini")
			log("Activity loaded")
		elseif count == 2 then
			Intent = jni.wrapClass("Class/Intent.ini")
			log("Intent loaded")
		elseif count == 3 then
			NotificationManager = jni.wrapClass("Class/Manager.ini")
			log("NotificationManager loaded")
		elseif count == 4 then
			NotificationBuilder = jni.wrapClass("Class/Notification.ini")
			log("NotificationBuilder loaded")
		elseif count == 5 then
			PendingIntent = jni.wrapClass("Class/PendingIntent.ini")
			log("PendingIntent loaded")
		elseif count == 6 then
			Context = jni.wrapClass("Class/Context.ini")
			log("Context loaded")

			log("Creating classes finished")
		end
	end

	

	if not switched and count == 7 then

		activity = ffi.C.SDL_AndroidGetActivity()
		log("Raw activity: %s", tostring(activity))
		activity = Activity(activity)
		log("Wrapped activity: %s", tostring(activity))

		switched = true
	elseif switched then
		timer2 = math.max(timer2-dt, 0)
	end

	if timer2 == 0 and not opened then
		opened = true


		--local context = Context.Context()
		local nm = activity:getSystemService(jni.toJavaString("notification"))
		nm = NotificationManager(nm)
		--log(tostring(nm))
		local builder = NotificationBuilder.Builder(jni.unwrapObject(activity))
		local notificationIntent = Intent.Intent4(jni.unwrapObject(activity), activity:getClass())
		local contentIntent = PendingIntent.getActivity(jni.unwrapObject(activity), 0, jni.unwrapObject(notificationIntent), 0)
		log("Initialized everything")

		--builder:setContentIntent(contentIntent)
		builder:setContentText(jni.toJavaString("Test text"))
		builder:setContentTitle(jni.toJavaString("Test title"))

		log("Building notification...")

		local notification = builder:build()

		log("Notify!")

		nm:notify(1,notification)

		--activity:startActivity(jni.unwrapObject(intent))
	end
end

function love.draw()
	love.graphics.print("Switching in " .. timer1, 10, 10)
	love.graphics.print("Loaded " .. count .. " Java classes", 10, 25)
	love.graphics.print("Browsing in " .. timer2, 10, 40)
	love.graphics.print(logMsg, 10, 60)
end
